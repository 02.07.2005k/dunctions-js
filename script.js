// Теоритичні питання 
// 1. Створення функції: Можна створити функцію в JavaScript, 
// використовуючи ключове слово "function", за яким слідує ім'я функції, 
// список параметрів у круглих дужках і тіло функції у фігурних дужках. 
// Функцію можна викликати, просто вказавши її ім'я з відповідними аргументами у круглих дужках.

// 2. Оператор return в JavaScript: 
// Оператор return використовується для повернення значення з функції.
// Він припиняє виконання функції та повертає вказане значення. 
// Return може бути використаний у будь-якому місці функції, 
// і він може повертати будь-який тип даних: примітивні значення, об'єкти, масиви тощо.

// 3. Параметри та аргументи в функціях: 
// Параметри - це змінні, які оголошуються у визначенні функції. 
// Вони використовуються для передачі значень у функцію. 
// Аргументи - це конкретні значення, передані у функцію при її виклику. 
// Кількість параметрів функції може бути різною від кількості аргументів, 
// які передаються при її виклику. Параметри використовуються для внутрішнього використання у функції, 
// тоді як аргументи використовуються для передачі даних в функцію.

// 4. Передача функції як аргумента в іншу функцію:
// У JavaScript функції є об'єктами, тому їх можна передавати як аргументи в інші функції.
// Це часто використовується для здійснення зворотного виклику (callback). 
// При передачі функції як аргумента в іншу функцію, 
// функція-параметр може бути викликана всередині цієї функції.

// Практичне Завдання 1
function divideNumbers(num1, num2) {
    if (num2 === 0) {
        return 'Ділення на нуль!';
    }
    return num1 / num2;
}

let num1;
do {
    num1 = parseFloat(prompt('Введіть перше число:'));
} while (isNaN(num1));

let num2;
do {
    num2 = parseFloat(prompt('Введіть друге число:'));
} while (isNaN(num2));

const result = divideNumbers(num1, num2);
console.log('Результат ділення:', result);

// Практичне Завдання 2
function isNumeric(value) {
    return !isNaN(parseFloat(value)) && isFinite(value);
}
  
 
function getNumberFromPrompt(promptMessage) {
    let input;
    do {
      input = prompt(promptMessage);
    } while (!isNumeric(input));
    return parseFloat(input);
  }
  
 
function getOperationFromPrompt() {
    let operation;
    do {
      operation = prompt('Введіть математичну операцію (+, -, *, /)');
    } while (!['+', '-', '*', '/'].includes(operation));
    return operation;
}
  
 
function calculate(num1, num2, operation) {
    switch (operation) {
      case '+':
        return num1 + num2;
      case '-':
        return num1 - num2;
      case '*':
        return num1 * num2;
      case '/':
        if (num2 === 0) {
          return 'Ділення на нуль!';
        } else {
          return num1 / num2;
        }
      default:
        return 'Такої операції не існує';
    }
}
 
const number1 = getNumberFromPrompt('Введіть перше число:');
const number2 = getNumberFromPrompt('Введіть друге число:');
const operation = getOperationFromPrompt();

const result2 = calculate(number1, number2, operation);
console.log('Результат:', result2);

// Завдання 3
const calculateFactorial = (number) => {
    let factorial = 1;
    for (let i = 2; i <= number; i++) {
        factorial *= i;
    }
    return factorial;
};

const userInput = parseInt(prompt('Введіть число:'));
const factorialResult = calculateFactorial(userInput);
alert(`Факторіал числа ${userInput} дорівнює: ${factorialResult}`);

